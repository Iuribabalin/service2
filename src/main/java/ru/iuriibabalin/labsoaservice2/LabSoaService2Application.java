package ru.iuriibabalin.labsoaservice2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LabSoaService2Application {

    public static void main(String[] args) {
        SpringApplication.run(LabSoaService2Application.class, args);
    }

}
