package ru.iuriibabalin.labsoaservice2.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
class SwaggerConfiguration {

    private ApiInfo apiInfo() {
        return new ApiInfo("Service-2",
                "Service-2 фасад для доступа к api Service-1",
                "1.0",
                "",
                "Бабалин Юрий",
                "",
                "");
    }

    @Bean(name = "swagger")
    public Docket api() {
        return new Docket(DocumentationType.OAS_30)
                .select()
                .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
                .paths(PathSelectors.any())
                .build()
                .enable(true)
                .apiInfo(apiInfo())
                .useDefaultResponseMessages(false);
    }
}