package ru.iuriibabalin.labsoaservice2.application.vehicle.model;

import io.swagger.v3.oas.annotations.media.Schema;
import java.time.LocalDateTime;
import lombok.Data;

@Data
public class VehicleDto {
    @Schema(name = "Идентификатор транспорта")
    private Integer id;
    @Schema(name = "Имя транспортного средства")
    private String name;
    @Schema(name = "Координаты транспортного средства")
    private CoordinatesDto coordinates;
    @Schema(name = "Дата создания")
    private LocalDateTime creationDate;
    @Schema(name = "Мощность двигателя")
    private Float enginePower;
    @Schema(name = "Тип пранспортного средства")
    private VehicleType type;
    @Schema(name = "Тип топлива")
    private FuelType fuelType;
}
