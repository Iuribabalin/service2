package ru.iuriibabalin.labsoaservice2.application.vehicle.web;

import io.swagger.annotations.Api;
import java.util.Collections;
import java.util.List;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.iuriibabalin.labsoaservice2.application.common.Endpoints;
import ru.iuriibabalin.labsoaservice2.application.vehicle.model.Page;
import ru.iuriibabalin.labsoaservice2.application.vehicle.model.VehicleDto;
import ru.iuriibabalin.labsoaservice2.application.vehicle.model.VehicleFilter;
import ru.iuriibabalin.labsoaservice2.application.vehicle.model.Wheels;

@Api(tags = "API Транспортных средств фасад")
@RestController
public class VehicleController {

    @PostMapping(Endpoints.VEHICLE)
    public VehicleDto addVehicle(@RequestBody VehicleDto vehicle) {
        return null;
    }

    @PostMapping(Endpoints.VEHICLE_SEARCH)
    public Page<VehicleDto> searchVehicle(@RequestBody VehicleFilter filterParams) {
        return Page.empty();
    }

    @PutMapping(Endpoints.VEHICLE)
    public void updateVehicle(@RequestBody VehicleDto vehicle) {
    }

    @PutMapping(Endpoints.FIX_DISTANCE)
    public void fixDistance(@PathVariable Long vehicleId) {
    }

    @PutMapping(Endpoints.ADD_WHEELS)
    public void addWheels(@RequestBody Wheels wheels, @PathVariable Long vehicleId) {
    }

    @DeleteMapping(Endpoints.GET_DELETE_VEHICLE)
    public void deleteVehicle(@PathVariable Integer vehicleId) {
    }

    @DeleteMapping(Endpoints.DELETE_VEHICLE_ENGINE_POWER)
    public void deleteVehicleEnginePower(@RequestParam Long enginePower) {
    }

    @DeleteMapping(Endpoints.DELETE_VEHICLE_TYPE)
    public void deleteVehicleType(@RequestParam String type) {
    }

    @GetMapping(Endpoints.GET_DELETE_VEHICLE)
    public VehicleDto getVehicle(@PathVariable Integer vehicleId) {
        return null;
    }

    @GetMapping(Endpoints.UNIQUE_VEHICLE_TYPE)
    public List<VehicleDto> getUniqueVehicleType() {
        return Collections.emptyList();
    }

}
