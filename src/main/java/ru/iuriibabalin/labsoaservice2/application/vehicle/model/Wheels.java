package ru.iuriibabalin.labsoaservice2.application.vehicle.model;

import lombok.Data;

@Data
public class Wheels {
    private Long number;
}
