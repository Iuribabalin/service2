package ru.iuriibabalin.labsoaservice2.application.vehicle.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class VehicleFilter extends PageableRequest{
    @Schema(name = "Имя транспортного средства")
    private String name;
    @Schema(name = "Координата x")
    private Long x;
    @Schema(name = "Координата y")
    private Long y;
    @Schema(name = "Мощность двигателя")
    private Float enginePower;
    @Schema(name = "Тип пранспортного средства")
    private VehicleType type;
    @Schema(name = "Тип топлива")
    private FuelType fuelType;
}
