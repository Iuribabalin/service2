package ru.iuriibabalin.labsoaservice2.application.vehicle.model;

public enum FuelType {
    GASOLINE,
    ALCOHOL,
    MANPOWER
}
