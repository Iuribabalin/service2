package ru.iuriibabalin.labsoaservice2.application.vehicle.model;

public enum VehicleType {
    HELICOPTER,
    DRONE,
    BICYCLE
}
