package ru.iuriibabalin.labsoaservice2.application.common;

public interface Endpoints {
    String VEHICLE = "/vehicle";
    String GET_DELETE_VEHICLE = "/vehicle/{vehicleId}";
    String VEHICLE_SEARCH = "/vehicle/search";

    String DELETE_VEHICLE_ENGINE_POWER = "/vehicle/clear";

    String DELETE_VEHICLE_TYPE = "/vehicle/clear/one";

    String UNIQUE_VEHICLE_TYPE = "/vehicle/unique/type";

    String FIX_DISTANCE = "/vehicle/fix-distance/{vehicleId}";

    String ADD_WHEELS = "/add-wheels/{vehicleId}/number-of-wheels";
}
